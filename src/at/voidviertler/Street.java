package at.voidviertler;

import at.voidviertler.models.Intersection;

public class Street {
    public Intersection startIntersection;
    public Intersection endIntersection;
    public String streetName;
    public int duration;
    public boolean endTrafficLight = false;

    public Street(Intersection startIntersection, Intersection endIntersection, String streetName, int duration) {
        this.startIntersection = startIntersection;
        this.endIntersection = endIntersection;
        this.streetName = streetName;
        this.duration = duration;
    }
}
