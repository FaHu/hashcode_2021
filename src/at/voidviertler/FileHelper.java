package at.voidviertler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class FileHelper {
    public static String readTextFileScanner(String url) {
        String result = "";

        try {
            URL sourceUrl = new URL(url);
            Scanner scanner = new Scanner(sourceUrl.openStream());
            StringBuilder sb = new StringBuilder();

            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
            }

            result = sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<String> readTextFile(String path) {
        Path pathToFile = Paths.get(path);
        try {
            return Files.readAllLines(pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void writeToFile(String fileName, StringBuilder contents) {
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(fileName, "UTF-8");
            writer.println(contents.toString());
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}