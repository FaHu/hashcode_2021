package at.voidviertler.calcs;

import at.voidviertler.Street;
import at.voidviertler.models.Car;

import java.util.HashMap;
import java.util.List;

public class StreetFrequencyCalc {

    public static HashMap<String, Integer> calcMap = new HashMap<>();

    public static void calcStreetFrequency(List<Car> cars) {
        for(Car car : cars) {
            for(Street street : car.path) {
                if(calcMap.containsKey(street.streetName)) {
                    var calc = calcMap.get(street.streetName);
                    calcMap.put(street.streetName, ++calc);
                } else {
                    calcMap.put(street.streetName, 1);
                }
            }
        }
    }
}
