package at.voidviertler;

import at.voidviertler.models.CityPlan;
import at.voidviertler.models.Intersection;


import static java.lang.System.out;

public class Simulator {
    public static int duration;
    public CityPlan cityPlan;

    public Simulator(int _duration, CityPlan cityPlan) {
        duration = _duration;
        this.cityPlan = cityPlan;
    }

    void run2() {
        out.println("Starting Run");

        out.println(cityPlan.intersections.size());
        for (Intersection intersection : cityPlan.intersections.values()) {
            out.println(intersection.id);
            out.println(1);

            intersection.greenLightStreetId = intersection.incomingStreets.stream().findFirst().get().streetName;

            for (Street street : intersection.incomingStreets) {
                // the first street has always green light
                if (intersection.greenLightStreetId.equals(street.streetName)) {
                    out.println(street.streetName + " " + (duration - 1));
                }
            }
        }
    }
}
