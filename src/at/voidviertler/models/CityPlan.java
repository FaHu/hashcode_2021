package at.voidviertler.models;

import at.voidviertler.Street;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CityPlan {
    public List<Car> cars;
    public List<Street> streets;
    public HashMap<Integer, Intersection> intersections;

    public CityPlan(List<Car> cars, List<Street> streets, HashMap<Integer, Intersection> intersections) {
        this.cars = cars;
        this.streets = streets;
        this.intersections = intersections;
    }
}
