package at.voidviertler.models;

import at.voidviertler.Simulator;
import at.voidviertler.Street;
import at.voidviertler.calcs.StreetFrequencyCalc;
import at.voidviertler.outputModels.ScheduleOutput;

import java.util.*;
import java.util.LinkedList;
import java.util.List;

public class Intersection {
    public int id;

    public List<Street> incomingStreets = new LinkedList<>();
    public List<Street> outgoingStreets = new LinkedList<>();

    public int value = 0;

    public String greenLightStreetId;

    public Intersection(int id) {
        this.id = id;
    }

    public void calculateValue() {
        for (Street incoming : incomingStreets) {
            if ( StreetFrequencyCalc.calcMap.containsKey(incoming.streetName)) {
                var result = StreetFrequencyCalc.calcMap.get(incoming.streetName);
                if (result != null) {
                    value += result;
                }
            }
        }
    }

    public ScheduleOutput calculateSchedule() {
        ScheduleOutput output = new ScheduleOutput(id);
        String maxStreetName = "";
        int slotDuration = 1;

        for (Street street : incomingStreets) {
            int streetWeight = StreetFrequencyCalc.calcMap.get(street.streetName);
            int weightDuration = (int) ((double)streetWeight / (double)value) * 100;

            weightDuration = (int)(Math.round(weightDuration/10.0) * 10);
            if(weightDuration > Simulator.duration) {
                weightDuration = Simulator.duration;
            }
            if(weightDuration >= 1) {
                output.schedule.put(street.streetName, slotDuration*weightDuration);
            }
        }

        output.schedule.put(maxStreetName, 1);
        output.streetCount = output.schedule.keySet().size();
        return output;
    }

}
