package at.voidviertler;

import at.voidviertler.calcs.StreetFrequencyCalc;
import at.voidviertler.models.Car;
import at.voidviertler.models.CityPlan;
import at.voidviertler.models.Intersection;
import at.voidviertler.outputModels.ScheduleOutput;

import java.util.*;

import static java.lang.System.out;

public class Main {
    public static String[] inputFileNames = {"a", "b", "c", "d", "e", "f"};
    //public static String[] inputFileNames = {"b"};

    public static void main(String[] args) {
        out.println("~~~ Hashcode 2021 ~~~");

        for (var inputFileName : inputFileNames) {
            StreetFrequencyCalc.calcMap = new HashMap<>();

            Simulator simulator = parseInputFile(inputFileName);
            simulator.run2();
        }
    }

    private static Simulator parseInputFile(String inputFileName) {
        out.println("Calculating : " + inputFileName);

        List<String> rawInput = FileHelper.readTextFile("./assets/" + inputFileName + ".txt");
        String[] line1 = rawInput.get(0).split(" ");

        /** Parse Metadata (line 1) */
        // D - the duration of the simulation, in seconds
        int durationOfSimulation = Integer.parseInt(line1[0]);  // in seconds

        // I - the number of intersections (with IDs from 0 to I -1)
        int numberOfIntersections = Integer.parseInt(line1[1]);

        // S - the number of streets
        int numberOfStreets = Integer.parseInt(line1[2]);

        // V - the number of cars
        int numberOfCars = Integer.parseInt(line1[3]);

        // F - the bonus points for each car that reaches its destination before time D.
        int bonusPointsPerCar = Integer.parseInt(line1[4]);

        List<Street> streets = new LinkedList<>();
        HashMap<Integer, Intersection> intersections = new HashMap();

        /** Parse streets */
        for (int i = 1; i <= numberOfStreets; i++) {
            String[] lineStreet = rawInput.get(i).split(" ");

            int intersectionStartId = Integer.parseInt(lineStreet[0]);

            Intersection startIntersection;
            if (intersections.containsKey(intersectionStartId)) {
                startIntersection = intersections.get(intersectionStartId);
            } else {
                startIntersection = new Intersection(intersectionStartId);
            }

            intersections.put(intersectionStartId, startIntersection);

            Intersection endIntersection;
            int intersectionEndId = Integer.parseInt(lineStreet[1]);
            if (intersections.containsKey(intersectionEndId)) {
                endIntersection = intersections.get(intersectionEndId);
            } else {
                endIntersection = new Intersection(intersectionEndId);
            }

            intersections.put(intersectionEndId, endIntersection);

            String streetName = lineStreet[2];
            int duration = Integer.parseInt(lineStreet[3]);

            Street street = new Street(startIntersection, endIntersection, streetName, duration);
            streets.add(street);

            startIntersection.outgoingStreets.add(street);
            endIntersection.incomingStreets.add(street);
        }

        /** Parse paths of cars */
        List<Car> cars = new LinkedList<>();
        for (int i = numberOfStreets + 1; i <= numberOfStreets + numberOfCars; i++) {
            String[] singleCarLine = rawInput.get(i).split(" ");

            int numberOfStreetsTheCarDrives = Integer.parseInt(singleCarLine[0]);

            /** Parse single route */
            List<Street> route = new LinkedList<>();
            for (int j = 1; j <= numberOfStreetsTheCarDrives; j++) {
                String streetName = singleCarLine[j];
                Street street = streets.stream().filter(s -> s.streetName.equals(streetName)).findFirst().get();
                route.add(street);
            }

            Car car = new Car(route);
            cars.add(car);
        }

        CityPlan cityPlan = new CityPlan(cars, streets, intersections);

        StreetFrequencyCalc.calcStreetFrequency(cityPlan.cars);

        for(Intersection intersection : cityPlan.intersections.values()) {
            intersection.calculateValue();
        }

        StringBuilder output = new StringBuilder();

        int count = 0;
        StringBuilder scheduleOutputBuilder = new StringBuilder();
        for(Intersection intersection : cityPlan.intersections.values()) {
            ScheduleOutput scheduleOutput = intersection.calculateSchedule();
            if (scheduleOutput.schedule.size() > 0) {
                scheduleOutput.writeOutput(scheduleOutputBuilder);
                count++;
            }
        }

        output.append(count + "\n");
        output.append(scheduleOutputBuilder.toString());


        FileHelper.writeToFile("./assets/out/" + inputFileName + "_out.txt", output);

        return new Simulator(durationOfSimulation, cityPlan);
    }
}
