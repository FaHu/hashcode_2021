package at.voidviertler.outputModels;

import at.voidviertler.FileHelper;

import java.util.HashMap;
import java.util.Map;

public class ScheduleOutput {

    public ScheduleOutput(int intersectionId) {
        this.intersectionId = intersectionId;
    }

    public int intersectionId;
    public int streetCount;
    public Map<String, Integer> schedule = new HashMap<>();

    public void writeOutput(StringBuilder output) {
        output.append(intersectionId + "\n");
        output.append(streetCount + "\n");

        schedule.forEach((key, value) -> output.append(key + " " + value + "\n"));
    }
}
